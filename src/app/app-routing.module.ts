import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ControlPanelComponent} from './control-panel/control-panel.component';
import {ArenasComponent} from './arenas/arenas.component';

const routes: Routes = [
  {
    path: '', component: ControlPanelComponent,
  },
  {
    path: 'arenas', component: ArenasComponent,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
