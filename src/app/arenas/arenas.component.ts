import { Component, OnInit } from '@angular/core';
import {Arena} from '../shared/model/arena';

@Component({
  selector: 'app-arenas',
  templateUrl: './arenas.component.html',
  styleUrls: ['./arenas.component.scss']
})
export class ArenasComponent implements OnInit {
  public arenas: Arena[] = [
    {
      name: 'The Forest',
      robots: [
        {
          id: 'robot1',
          name: 'Robot 1'
        }
      ]
    }
  ];
  constructor() { }

  ngOnInit() {
  }

}
