import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ArenasComponent } from './arenas.component';
import { ArenaComponent } from './components/arena/arena.component';
import {SharedModule} from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
  ],
  declarations: [ArenasComponent, ArenaComponent]
})
export class ArenasModule { }
