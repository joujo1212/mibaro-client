import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Arena} from '../../../shared/model/arena';
import {Robot} from '../../../shared/model/robot';
import {StoreService} from '../../../shared/store.service';
import {Action} from '../../../shared/model/action.enum';
import {Message} from '../../../shared/model/message';
import {RobotService} from '../../../shared/robot.service';
import {SocketService} from '../../../shared/socket.service';
import {Event} from '../../../shared/model/event.enum';
import {Router} from '@angular/router';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-arena',
  templateUrl: './arena.component.html',
  styleUrls: ['./arena.component.scss']
})
export class ArenaComponent implements OnInit, OnDestroy {
  @Input() arena: Arena;
  public isPinging = false;
  private robotPingTimeout;
  public robotActive;
  private robotSub: Subscription;

  constructor(private storeService: StoreService, private robotService: RobotService, private socketService: SocketService, private router: Router) { }

  ngOnInit() {
  }

  ngOnDestroy() {
    if (this.robotSub) {
      this.robotSub.unsubscribe();
    }
  }

  selectRobot(robot: Robot) {
    this.robotActive = false;
    this.isPinging = false;
    // this.activeRobot = null;
    this.storeService.activeRobot.next(robot);
    this.connectToRobot();
    this.pingRobot();
  }

  public pingRobot() {
    this.robotPingTimeout = setTimeout(() => {
      this.robotActive = false;
      this.isPinging = false;
    }, 5000);
    this.isPinging = true;
    this.robotService.ping();
  }

  private connectToRobot() {
    this.robotSub = this.socketService.onRobotChannel(this.storeService.activeRobot.getValue().id)
      .subscribe((m: Message) => {
        switch (m.action) {
          case Action.PONG:
            // TODO move all data to store
            this.robotActive = true;
            clearTimeout(this.robotPingTimeout);
            this.isPinging = false;
            this.robotService.getBasicInfo();
            break;
          case Action.BASIC_INFO:
            this.storeService.activeRobot.next({...this.storeService.activeRobot.getValue(), basicInfo: m.data});
            // when we have basic info, redirect to the game
            this.router.navigateByUrl('/');
            break;
          default:
            break;
        }
      });
  }

}
