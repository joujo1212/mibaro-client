import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {Robot} from './model/robot';
import {Settings} from './model/settings';

@Injectable({
  providedIn: 'root'
})
export class AudioManager {
  public static SOUNDS = {
    btnHover: new Audio('/assets/sounds/btn_hover.wav'),
    btnClick: new Audio('/assets/sounds/btn_click.wav')
  };
  public static MUSIC = {
    music1: new Audio('/assets/sounds/music1.mp3')
  };
  private musicVolume = 1;
  private soundVolume = 1;

  constructor() { }

  /**
   * Set volume for music
   * @param {number} volume Number from range 0 - 1
   */
  public setMusicVolume(volume: number): void {
    this.musicVolume = volume;
    Object.keys(AudioManager.MUSIC).forEach(key => {
      AudioManager.MUSIC[key].volume = volume;
    });
  }

  /**
   * Set volume for sound effects
   * @param {number} volume Number from range 0 - 1
   */
  public setSoundVolume(volume: number): void {
    this.soundVolume = volume;
  }

  public getSoundVolume(): number {
    return this.soundVolume;
  }

  public getMusicVolume(): number {
    return this.musicVolume;
  }

  public playSound(sound: HTMLAudioElement): void {
    sound.volume = this.soundVolume;
    sound.play();
  }

  public stopSound(sound: HTMLAudioElement): void {
    sound.pause();
    sound.currentTime = 0;
  }

  public playMusic(music: HTMLAudioElement, loop = false): void {
    music.volume = this.musicVolume;
    music.loop = loop;
    music.play();
  }

  public stopMusic(music: HTMLAudioElement): void {
    music.pause();
    music.currentTime = 0;
  }
}
