import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from './material/material.module';
import {BgComponent} from './components/bg/bg.component';
import { ButtonDirective } from './components/button.directive';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule
  ],
  declarations: [BgComponent, ButtonDirective],
  providers: [],
  exports: [BgComponent, ButtonDirective]
})
export class SharedModule { }
