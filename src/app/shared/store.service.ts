import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {Robot} from './model/robot';
import {Settings} from './model/settings';

@Injectable({
  providedIn: 'root'
})
export class StoreService {
  public robots: BehaviorSubject<Robot[]> = new BehaviorSubject<Robot[]>(
    [
      {name: 'Robot 1', id: 'robot1'},
      {name: 'Robot 2', id: 'robot2 '}
    ]
  );
  public readonly activeRobot: BehaviorSubject<Robot> = new BehaviorSubject<Robot>(null);
  public readonly settings: BehaviorSubject<Settings> = new BehaviorSubject<Settings>({
    speed: 0.7,
    trimmer: 0,
    allowJoystick: true,
    allowKeyboard: true
  });
  constructor() { }

  public updateSetting(newSettings: Partial<Settings>) {
    let settings = this.settings.getValue();
    settings = {...settings, ...newSettings};
    this.settings.next(settings);
  }
}
