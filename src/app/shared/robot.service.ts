import { Injectable } from '@angular/core';
import {MoveData} from './model/mode-data';
import {Action} from './model/action.enum';
import {SocketService} from './socket.service';
import {StoreService} from './store.service';
import {Robot} from './model/robot';

@Injectable({
  providedIn: 'root'
})
export class RobotService {

  constructor(private socketService: SocketService, private storeService: StoreService) { }

  public ping() {
    const robot: Robot = this.storeService.activeRobot.getValue();
    this.socketService.send('robot/' + robot.id, {action: Action.PING});
  }

  public move(data: MoveData): void {
    const robot: Robot = this.storeService.activeRobot.getValue();
    this.socketService.send('robot/' + robot.id, {action: Action.MOVE, data});
  }

  public getData() {
    const robot: Robot = this.storeService.activeRobot.getValue();
    this.socketService.send('robot/' + robot.id, {action: Action.GET_BATTERY});
  }

  public sendCommand(command: string) {
    const robot: Robot = this.storeService.activeRobot.getValue();
    this.socketService.send('robot/' + robot.id, {action: Action.SEND_COMMAND, data: {command}});
  }

  public takePhoto(resolution: string = '-w 640 -h 480', quality = 50, awb = 'auto') {
    // const command = `raspistill -o /home/pi/robot/image.jpg  --nopreview --timeout 1 ${resolution} -q ${quality}`;
    const command = `raspistill -o /home/pi/robot/image.jpg  -vf -hf --nopreview --timeout 500 --awb ${awb} ${resolution} -q ${quality}`;
    const robot: Robot = this.storeService.activeRobot.getValue();
    this.socketService.send('robot/' + robot.id, {action: Action.TAKE_PHOTO, data: {command}});
  }

  public turnCameraOn() {
    const robot: Robot = this.storeService.activeRobot.getValue() || {};
    this.socketService.send('robot/' + robot.id, {action: Action.TURN_CAMERA_ON});
  }

  public turnCameraOff() {
    const robot: Robot = this.storeService.activeRobot.getValue() || {};
    this.socketService.send('robot/' + robot.id, {action: Action.TURN_CAMERA_OFF});
  }

  public getBasicInfo() {
    const robot: Robot = this.storeService.activeRobot.getValue();
    this.socketService.send('robot/' + robot.id, {action: Action.GET_BASIC_INFO});
  }

  public fire(): void {
    const robot: Robot = this.storeService.activeRobot.getValue();
    this.socketService.send('robot/' + robot.id, {action: Action.FIRE});
  }

  public laserUp(): void {
    const robot: Robot = this.storeService.activeRobot.getValue();
    this.socketService.send('robot/' + robot.id, {action: Action.LASER_UP});
  }

  public laserDown(): void {
    const robot: Robot = this.storeService.activeRobot.getValue();
    this.socketService.send('robot/' + robot.id, {action: Action.LASER_DOWN});
  }

  public toggleLight(): void {
    const robot: Robot = this.storeService.activeRobot.getValue();
    this.socketService.send('robot/' + robot.id, {action: Action.LIGHT_TOGGLE});
  }

  public getTemperature(): void {
    const robot: Robot = this.storeService.activeRobot.getValue();
    this.socketService.send('robot/' + robot.id, {action: Action.TEMPERATURE});
  }
}
