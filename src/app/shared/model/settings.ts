export interface Settings {
  speed: number;
  trimmer: number;
  allowJoystick: boolean;
  allowKeyboard: boolean;
}
