import {Action} from './action.enum';

export interface Message {
  robotId?: string; // or ALL for message to all robots
  data?: any;
  action?: Action;
}
