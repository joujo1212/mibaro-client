export interface Robot {
  id?: string;
  name?: any;
  basicInfo?: {localIp: string, publicIp: string};
}
