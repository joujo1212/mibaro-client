import {Robot} from './robot';

export interface Arena {
  id?: string;
  name?: string;
  robots: Robot[];
}
