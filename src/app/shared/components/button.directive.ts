import {Directive, HostListener} from '@angular/core';
import {AudioManager} from '../audio.manager';

@Directive({
  selector: '[appButton]'
})
export class ButtonDirective {

  constructor(private audioManager: AudioManager) { }

  @HostListener('mouseenter') onMouseEnter() {
    this.audioManager.playSound(AudioManager.SOUNDS.btnHover);
  }

  @HostListener('mouseleave') onMouseLeave() {
    this.audioManager.stopSound(AudioManager.SOUNDS.btnHover);
  }

  @HostListener('click') onClick() {
    this.audioManager.playSound(AudioManager.SOUNDS.btnClick);
  }
}
