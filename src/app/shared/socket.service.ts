import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Message } from './model/message';
import { Event } from './model/event.enum';

import * as socketIo from 'socket.io-client';

const SERVER_URL = 'http://159.65.205.222:8090';

@Injectable()
export class SocketService {
  private socket;

  public initSocket(): void {
    this.socket = socketIo(SERVER_URL);
  }

  public send(channel: string, message: Message): void {
    this.socket.emit(channel, message);
  }

  public onMessage(): Observable<Message> {
    return new Observable<Message>(observer => {
      this.socket.on('message', (data: Message) => observer.next(data));
    });
  }

  public onEvent(event: Event): Observable<any> {
    return new Observable<Event>(observer => {
      this.socket.on(event, () => observer.next());
    });
  }

  public onRobotChannel(robotId: string): Observable<Message> {
    return new Observable<Message>(observer => {
      this.socket.on('robot/' + robotId, (data: Message) => observer.next(data));
    });
  }
}
