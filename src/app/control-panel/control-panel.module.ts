import { NgModule } from '@angular/core';
import { ControlPanelComponent } from './control-panel.component';
import {MoveControllerComponent} from './shared/components/move-controller/move-controller.component';
import {MaterialModule} from '../shared/material/material.module';
import {CommonModule} from '@angular/common';
import {SensorDataComponent} from './shared/components/sensor-data/sensor-data.component';
import { KeyboardControllerComponent } from './shared/components/keyboard-controller/keyboard-controller.component';
import {TerminalComponent} from './shared/components/terminal/terminal.component';
import {FormsModule} from '@angular/forms';
import {SafeHtml} from '../shared/safe-html-pipe';
import {PhotoVisionComponent} from './shared/components/photo-vision/photo-vision.component';
import {VideoVisionComponent} from './shared/components/video-vision/video-vision.component';
import { SettingsComponent } from './shared/components/settings/settings.component';
import { BgComponent } from '../shared/components/bg/bg.component';
import {SharedModule} from '../shared/shared.module';
import {RouterModule} from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    SharedModule,
    RouterModule
  ],
  declarations: [
    ControlPanelComponent,
    MoveControllerComponent,
    SensorDataComponent,
    KeyboardControllerComponent,
    TerminalComponent,
    SafeHtml,
    PhotoVisionComponent,
    VideoVisionComponent,
    SettingsComponent
  ],
  providers: []
})
export class ControlPanelModule { }
