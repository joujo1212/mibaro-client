import {Component, Input, OnInit} from '@angular/core';
import {Action} from '../../../../shared/model/action.enum';
import {SocketService} from '../../../../shared/socket.service';
import {RobotService} from '../../../../shared/robot.service';
import {Message} from '../../../../shared/model/message';
import {StoreService} from '../../../../shared/store.service';

@Component({
  selector: 'app-terminal',
  templateUrl: './terminal.component.html',
  styleUrls: ['./terminal.component.css']
})
export class TerminalComponent implements OnInit {
  private activeRobot;
  public batteryLevel = 0;
  public command = '';
  public terminalStack = '';

  constructor(private robotService: RobotService, private socketService: SocketService, private storeService: StoreService) { }

  ngOnInit() {

    this.storeService.activeRobot.subscribe(activeRobot => {
      this.activeRobot = activeRobot;
      this.socketService.onRobotChannel(this.activeRobot.id)
        .subscribe((m: Message) => {
          switch (m.action) {
            case Action.COMMAND_RESPONSE:
              console.log(m);
              this.terminalStack = m.data.response.replace(/\n/g, '<br/>') + '<br/>' + this.terminalStack;
              break;
          }
        });
    });
  }

  public sendCommand() {
    this.robotService.sendCommand(this.command);
    this.command = '';
  }
}
