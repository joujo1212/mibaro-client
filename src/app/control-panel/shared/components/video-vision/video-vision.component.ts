import {Component, ElementRef, Input, OnDestroy, OnInit, ViewChild, ViewRef} from '@angular/core';
import {Action} from '../../../../shared/model/action.enum';
import {SocketService} from '../../../../shared/socket.service';
import {RobotService} from '../../../../shared/robot.service';
import {Message} from '../../../../shared/model/message';
import {StoreService} from '../../../../shared/store.service';
import {Robot} from '../../../../shared/model/robot';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-video-vision',
  templateUrl: './video-vision.component.html',
  styleUrls: ['./video-vision.component.scss']
})
export class VideoVisionComponent implements OnInit, OnDestroy {
  public activeRobot: Robot;
  private robotSub: Subscription;
  public IP: string;
  public showSettings = false;
  public isCameraOn = false;
  public isVrOn = false;
  public waitingForVideo = false;
  public temperature = 0;
  private tempInterval;
  private activeRobotSub: Subscription;

  constructor(private robotService: RobotService, private socketService: SocketService, private storeService: StoreService) { }

  ngOnInit() {
    this.tempInterval = setInterval(() => {
      this.robotService.getTemperature();
    }, 5000);
    this.activeRobotSub = this.storeService.activeRobot.subscribe(activeRobot => {
      this.activeRobot = activeRobot;
      if (this.activeRobot.basicInfo) {
        this.IP = this.activeRobot.basicInfo.localIp;
      }
      if (this.robotSub) {
        this.robotSub.unsubscribe();
      }
      this.robotSub = this.socketService.onRobotChannel(this.activeRobot.id)
        .subscribe((m: Message) => {
          switch (m.action) {
            case Action.TEMPERATURE_RESPONSE:
              this.temperature = m.data.temperature;
          }
        });
      console.log('posielam');
      if (!this.isCameraOn) {
        this.toggleCamera();
      }
    });
  }

  ngOnDestroy() {
    console.log('koncim');
    this.robotService.turnCameraOff();
    clearInterval(this.tempInterval);
    this.activeRobotSub.unsubscribe();
  }

  public toggleSettings() {
    this.showSettings = !this.showSettings;
  }

  public toggleVR() {
    this.isVrOn = !this.isVrOn;
  }

  public toggleCamera() {
    if (this.isCameraOn) {
      this.robotService.turnCameraOff();
      this.waitingForVideo = false;
    } else {
      this.robotService.turnCameraOn();
      this.waitingForVideo = true;
    }
    this.isCameraOn = !this.isCameraOn;
  }

  public tryToLoadImage() {
    console.log('tryToLoadImage');
    this.isCameraOn = false;
    setTimeout(() => {
      this.isCameraOn = true;
      console.log('uz', this.IP);
      this.waitingForVideo = false;
    }, 1000);
  }

  public downloadPhoto() {
    const link = document.createElement('a');
    link.download = `video-snapshot-${Date.now()}.jpg`;
    link.href = 'http://' + this.IP + ':8080/?action=snapshot';
    link.target = '_blank';

    link.click();
  }
}
