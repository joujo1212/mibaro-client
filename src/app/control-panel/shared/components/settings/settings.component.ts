import {Component, OnDestroy, OnInit} from '@angular/core';
import {StoreService} from '../../../../shared/store.service';
import {Subscription} from 'rxjs';
import {Settings} from '../../../../shared/model/settings';
import {AudioManager} from '../../../../shared/audio.manager';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit, OnDestroy {
  public trimmerValue = 0;
  public soundsVolume = 0;
  public musicVolume = 0;
  public allowKeyboard = true;
  public allowJoystick = true;
  public speed = 0.8;
  private storeServiceSub: Subscription;

  constructor(private storeService: StoreService, private audioManager: AudioManager) { }

  ngOnInit() {
    this.storeServiceSub = this.storeService.settings.asObservable().subscribe((settings: Settings) => {

    });
    this.soundsVolume = this.audioManager.getSoundVolume();
    this.musicVolume = this.audioManager.getMusicVolume();
  }

  ngOnDestroy() {
    this.storeServiceSub.unsubscribe();
  }

  public handleChangeTrimmerValue({value}) {
    console.log(event);
    this.trimmerValue = value;
    this.storeService.updateSetting({trimmer: value});
  }

  public handleChangeSoundsVolume({value}) {
    console.log(event);
    this.soundsVolume = value;
    this.audioManager.setSoundVolume(value);
    this.audioManager.stopSound(AudioManager.SOUNDS.btnHover);
    this.audioManager.playSound(AudioManager.SOUNDS.btnHover);
  }

  public handleChangeMusicVolume({value}) {
    console.log(event);
    this.musicVolume = value;
    this.audioManager.setMusicVolume(value);
  }

  public handleChangeSpeed({value}) {
    console.log(event);
    this.speed = value;
    this.storeService.updateSetting({speed: value});
  }

  public renderThumb(value: number) {
    return `${Math.round(value * 100)}%`;
  }

  public controlWithKeyboardChanged({checked}) {
    this.allowKeyboard = checked;
    this.storeService.updateSetting({allowKeyboard: checked});
  }

  public controlWithJoystickChanged({checked}) {
    this.allowJoystick = checked;
    this.storeService.updateSetting({allowJoystick: checked});
  }
}
