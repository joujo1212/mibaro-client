import {Component, OnDestroy, OnInit} from '@angular/core';
import {RobotService} from '../../../../shared/robot.service';
import {Subscription} from 'rxjs';
import {StoreService} from '../../../../shared/store.service';
import {Settings} from '../../../../shared/model/settings';
import {MoveData} from '../../../../shared/model/mode-data';

const KEY_CODES = {
  up: 38,
  down: 40,
  left: 37,
  right:  39,
  space:  32,
  shift: 16,
  ctrl: 17,
  l: 76
};

@Component({
  selector: 'app-keyboard-controller',
  templateUrl: './keyboard-controller.component.html',
  styleUrls: ['./keyboard-controller.component.scss']
})
export class KeyboardControllerComponent implements OnInit, OnDestroy {
  public move: MoveData = new MoveData();
  private settings: Settings;
  private settingsSub: Subscription;
  private keys = {
    [KEY_CODES.up]: true,
    [KEY_CODES.down]: true,
    [KEY_CODES.left]: true,
    [KEY_CODES.right]: true,
    [KEY_CODES.space]: true,
    [KEY_CODES.shift]: true,
    [KEY_CODES.ctrl]: true,
    [KEY_CODES.l]: true,
  };

  constructor(private robotService: RobotService, private storeService: StoreService) { }

  ngOnInit() {
    this.settingsSub = this.storeService.settings.asObservable().subscribe((settings: Settings) => {
      this.settings = settings;
    });
    window.onkeyup = (e) => {
      if (this.settings.allowKeyboard) {
        const key = e.keyCode ? e.keyCode : e.which;
        console.log(key);
        switch (key) {
          case KEY_CODES.up:
          case KEY_CODES.down:
          case KEY_CODES.left:
          case KEY_CODES.right:
            this.keys[key] = true;
            this.move.dir = this.move.left = this.move.right = 0;
            this.sendMove();
            break;
          case KEY_CODES.space:
          case KEY_CODES.ctrl:
          case KEY_CODES.shift:
          case KEY_CODES.l:
            this.keys[key] = true;
        }
        e.preventDefault();
      }
    };

    window.onkeydown = (e) => {
      if (this.settings.allowKeyboard) {
        const key = e.keyCode ? e.keyCode : e.which;
        if (!this.keys[key]) {
          e.preventDefault();
          return;
        }

        console.log(key);
        switch (key) {
          case KEY_CODES.up:
            this.move.dir = 1;
            if (this.settings.trimmer < 0) {
              this.move.left = this.settings.speed * Math.abs(1 + this.settings.trimmer);
              this.move.right = this.settings.speed;
            } else if (this.settings.trimmer > 0) {
              this.move.left = this.settings.speed;
              this.move.right = this.settings.speed * Math.abs(1 - this.settings.trimmer);
            } else {
              this.move.left = this.move.right = this.settings.speed;
            }
            this.sendMove();
            break;
          case KEY_CODES.down:
            this.move.dir = -1;
            this.move.left = this.move.right = this.settings.speed;
            this.sendMove();
            break;
          case KEY_CODES.left:
            this.move.dir = 1;
            this.move.left = 0;
            this.move.right = this.settings.speed;
            this.sendMove();
            break;
          case KEY_CODES.right:
            this.move.dir = 1;
            this.move.left = this.settings.speed;
            this.move.right = 0;
            this.sendMove();
            break;
          case KEY_CODES.space:
            this.robotService.fire();
            break;
          case KEY_CODES.shift:
            this.robotService.laserUp();
            break;
          case KEY_CODES.ctrl:
            this.robotService.laserDown();
            break;
          case KEY_CODES.l:
            this.robotService.toggleLight();
            break;
        }
        e.preventDefault();
        this.keys[key] = false;
      }
    };
  }

  ngOnDestroy() {
    this.settingsSub.unsubscribe();
  }

  private sendMove() {
    this.robotService.move(this.move);
  }
}
