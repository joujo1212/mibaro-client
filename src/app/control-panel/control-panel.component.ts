import {Component, OnDestroy, OnInit} from '@angular/core';
import {Message} from '../shared/model/message';
import {SocketService} from '../shared/socket.service';
import {RobotService} from '../shared/robot.service';
import {StoreService} from '../shared/store.service';
import {Robot} from '../shared/model/robot';
import {Settings} from '../shared/model/settings';
import {Subscription} from 'rxjs';
import {AudioManager} from '../shared/audio.manager';
import {Router} from '@angular/router';

@Component({
  selector: 'app-chat',
  templateUrl: './control-panel.component.html',
  styleUrls: ['./control-panel.component.scss']
})
export class ControlPanelComponent implements OnInit, OnDestroy {
  messages: Message[] = [];
  ioConnection: any;
  public activeRobot: Robot;
  public robots: Robot[];
  isPinging = false;
  robotActive = false;
  robotPingTimeout;
  selectedPanels = [3, 5, 6];
  panels = [
    {id: 1, name: 'System control'},
    {id: 2, name: 'Terminal'},
    {id: 3, name: 'Move controller'},
    {id: 4, name: 'Sensor data'},
    {id: 5, name: 'Photo Vision'},
    {id: 6, name: 'Video Vision'},
  ];
  private settingsSub: Subscription;
  public settings: Settings;

  constructor(private socketService: SocketService, private storeService: StoreService, private robotService: RobotService,
              private audioManager: AudioManager, private router: Router) {
  }

  ngOnInit(): void {
    this.audioManager.playMusic(AudioManager.MUSIC.music1, true);
    this.storeService.robots.subscribe(robots => {
      this.robots = robots;
    });
    this.storeService.activeRobot.subscribe((activeRobot: Robot) => {
      if (!activeRobot) {
        this.router.navigateByUrl('/arenas');
        return;
      }
      this.activeRobot = activeRobot;
    });

    this.settingsSub = this.storeService.settings.asObservable().subscribe((settings: Settings) => {
      this.settings = settings;
    });
  }

  ngOnDestroy() {
    this.audioManager.stopMusic(AudioManager.MUSIC.music1);
    this.settingsSub.unsubscribe();
  }

  public pingRobot() {
    this.robotPingTimeout = setTimeout(() => {
      this.robotActive = false;
      this.isPinging = false;
    }, 5000);
    this.isPinging = true;
    this.robotService.ping();
  }

  public preventDefault(event) {
    event.preventDefault();
    return false;
  }
}
