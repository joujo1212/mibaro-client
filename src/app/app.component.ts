import {Component, OnInit} from '@angular/core';
import {Event} from './shared/model/event.enum';
import {SocketService} from './shared/socket.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'app';

  constructor(private socketService: SocketService) {}

  ngOnInit() {
    // init websocket once per app
    this.initIoConnection();
  }

  private initIoConnection(): void {
    this.socketService.initSocket();

    // this.ioConnection = this.socketService.onMessage()
    //   .subscribe((message: Message) => {
    //     this.messages.push(message);
    //   });

    this.socketService.onEvent(Event.CONNECT)
      .subscribe(() => {
        console.log('connected to server');
      });

    this.socketService.onEvent(Event.DISCONNECT)
      .subscribe(() => {
        console.log('disconnected from server');
      });
  }
}
