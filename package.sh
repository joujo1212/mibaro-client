#!/bin/bash

#  constant part!
RED='\033[1;31m'
BLUE='\033[1;34m'
GREEN='\033[1;32m'
NC='\033[0m' # No Color

set -e
printf "${BLUE}Packaging to electron app...${NC}\n"

printf "${BLUE}Building angular app...${NC}\n"
ng build --prod

printf "${BLUE}Compiling & packaging electron native app...${NC}\n"
electron-packager . --platform=darwin --overwrite

printf "${GREEN}Packaging done!${NC}\n"
